﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using DHC.Models;
using HtmlAgilityPack;

namespace DHC.Controllers
{
    public class HomeController : Controller
    {
        string _league = "P";
        string _team = "diehard";
        bool _winter = false;
        string _scheduleUrl;
        string _statsUrl;
        string _standingsUrl;

        public HomeController()
        {
            _scheduleUrl = _winter ? "http://www.mbhl.ca/dmts/shared/masterxmldw.xml" : "http://www.mbhl.ca/dmts/shared/masterxmld.xml";
            _statsUrl = _winter ? "http://www.mbhl.ca/dmts/shared/stats/wfDiehard%20Hockey%20Club.htm" : "http://www.mbhl.ca/dmts/shared/stats/Diehard%20Hockey%20Club.htm";
            _standingsUrl = _winter ? "http://www.mbhl.ca/dmts/shared/stats/wfGstandings.htm" : "http://www.mbhl.ca/dmts/shared/stats/Pstandings.htm";
        }

        [OutputCache( CacheProfile = "DefaultCache" )]
        public ActionResult Schedule()
        {
            IEnumerable<Game> games = GetGames();

            ViewBag.Games = games.Where( x => x.league == _league && (x.hometeam.ToLower().Contains( _team ) || x.visitteam.ToLower().Contains( _team )) ).ToList();

            return View();
        }

        [OutputCache( CacheProfile = "DefaultCache" )]
        public ActionResult LeagueSchedule()
        {
            IEnumerable<Game> games = GetGames();

            ViewBag.Games = games.Where( x => x.league == _league ).ToList();

            return View("Schedule");
        }

        [OutputCache( CacheProfile = "DefaultCache" )]
        public ActionResult Stats()
        {
            WebClient client = new WebClient();
            string html = client.DownloadString( _statsUrl );

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml( html );

            var rows = from row in doc.DocumentNode.Descendants( "tr" )
                       where row.Descendants("td").Any(x => x.Attributes["class"] != null && x.Attributes["class"].Value.StartsWith("xl711"))
                       && row.Attributes["style"].Value != null && row.Attributes["style"].Value != "display:none"
                       select row;

            ViewBag.Players = GetPlayerStats( rows );
            ViewBag.Goalies = GetGoalieStats( rows );

            return View();
        }

        [OutputCache( CacheProfile = "DefaultCache" )]
        public ActionResult Standings()
        {
            WebClient client = new WebClient();
            string html = client.DownloadString( _standingsUrl );

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml( html );

            var rows = from row in doc.DocumentNode.Descendants( "tr" )
                       where row.Descendants("td").Any( x => x.Attributes["class"] != null && x.Attributes["class"].Value.StartsWith("xl66") )
                       && row.Attributes["style"].Value != null && row.Attributes["style"].Value != "display:none"
                       select row;

            List<Standing> standings = new List<Standing>();
            foreach( var row in rows )
            {
                var cols = row.Descendants("td").ToArray();

                var standing = new Standing
                {
                    Position = cols[0].InnerText,
                    Division = cols[1].InnerText,
                    Team = cols[2].InnerText,
                    GP = cols[3].InnerText,
                    Wins = cols[4].InnerText,
                    Loses = cols[5].InnerText,
                    Ties = cols[6].InnerText,
                    Points = cols[7].InnerText,
                    GF = cols[8].InnerText,
                    GA = cols[9].InnerText,
                    Diff = cols[10].InnerText,
                    SF = cols[11].InnerText,
                    SA = cols[12].InnerText,
                    Last5 = cols[13].InnerText
                };

                standings.Add( standing );
            }

            ViewBag.Standings = standings;

            return View();
        }

        private List<Player> GetPlayerStats( IEnumerable<HtmlNode> rows )
        {
            List<Player> players = new List<Player>();
            foreach( var row in rows )
            {
                var cols = row.Descendants( "td" ).ToArray();

                if( !cols[1].InnerText.Contains("Team") && cols[3].InnerText != "0" && cols[3].InnerText != "&nbsp;" && !cols[12].InnerText.Contains( "%" ) )
                {
                    var player = new Player
                    {
                        Rank = cols[0].InnerText,
                        Number = cols[1].InnerText,
                        Name = cols[2].InnerText,
                        GP = cols[3].InnerText,
                        Goals = cols[4].InnerText,
                        Assits = cols[5].InnerText,
                        Points = cols[6].InnerText,
                        PIM = cols[7].InnerText,
                        PPG = cols[8].InnerText,
                        SHG = cols[9].InnerText,
                        GWG = cols[10].InnerText,
                        PointsPerGame = cols[11].InnerText,
                        Playoffs = cols[12].InnerText
                    };

                    players.Add( player );
                }
            }
            return players;
        }

        private List<Goalie> GetGoalieStats( IEnumerable<HtmlNode> rows )
        {
            List<Goalie> goalies = new List<Goalie>();
            foreach( var row in rows )
            {
                var cols = row.Descendants( "td" ).ToArray();

                if( cols[3].InnerText != "0" && cols[3].InnerText != "&nbsp;" && cols[12].InnerText.Contains( "%" ) )
                {
                    var goalie = new Goalie
                    {
                        Rank = cols[0].InnerText,
                        Name = cols[2].InnerText,
                        GP = cols[3].InnerText,
                        Wins = cols[4].InnerText,
                        Loses = cols[5].InnerText,
                        Ties = cols[6].InnerText,
                        GA = cols[7].InnerText,
                        SO = cols[8].InnerText,
                        SA = cols[9].InnerText,
                        AVG = cols[11].InnerText,
                        SavePourcentage = cols[12].InnerText
                    };

                    goalies.Add( goalie );
                }
            }
            return goalies;
        }

        private IEnumerable<Game> GetGames()
        {
            IEnumerable<Game> Games = null;

            WebClient client = new WebClient();
            string text = client.DownloadString( _scheduleUrl );

            XmlSerializer serializer = new XmlSerializer( typeof( Schedule ) );
            using( TextReader reader = new StringReader( text ) )
            {
                Schedule result = (Schedule)serializer.Deserialize( reader );
                Games = result.Games.Where( x => !string.IsNullOrEmpty( x.gamenumber ) && !string.IsNullOrEmpty( x.hometeam ) && !string.IsNullOrEmpty( x.visitteam ) );
            }

            return Games;
        }
    }
}