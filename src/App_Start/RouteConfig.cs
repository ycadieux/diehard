﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using LowercaseRoutesMVC4;

namespace DHC
{
    public class RouteConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.IgnoreRoute( "{resource}.axd/{*pathInfo}" );

            routes.MapRouteLowercase(
                name: "league-schedule",
                url: "league-schedule",
                defaults: new { controller = "Home", action = "LeagueSchedule" }
            );

            routes.MapRouteLowercase(
                name: "Default",
                url: "{action}",
                defaults: new { controller = "Home", action = "Schedule" }
            );
        }
    }
}
