﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace DHC.Models
{
    public class Schedule
    {
        [XmlElement( "game", Type = typeof( Game ) )]
        public Game[] Games { get; set; }
    }

    public class Game
    {
        public string gamenumber { get; set; }
        public string league { get; set; }
        public string day { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string hometeam { get; set; }
        public string homescore { get; set; }
        public string visitteam { get; set; }
        public string visitscore { get; set; }
        public string arena { get; set; }
        public string timekeeper { get; set; }
        public string manager { get; set; }
        public string stats { get; set; }
        public string xml_date { get; set; }
        public string xml_time { get; set; }

        public bool HasTime
        {
            get
            {
                int time;
                return int.TryParse( this.time, out time );
            }
        }

        public DateTime? DateTime
        {
            get
            {
                int days;
                int hour;
                int minute;
                if( int.TryParse( date, out days ))
                {
                    if( int.TryParse( time.Substring( 0, 2 ), out hour ) && int.TryParse( time.Substring( 2, 2 ), out minute ) )
                    {
                        return new DateTime( 1900, 1, 1, hour, minute, 0 ).AddDays( days - 2 );
                    }
                    else
                    {
                        return new DateTime( 1900, 1, 1 ).AddDays( days - 2 );
                    }

                    
                }

                return null;
            }
        }

        public string ShortDateTimeDisplay
        {
            get
            {
                if (DateTime.HasValue)
                {
                    var cultureInfo = new System.Globalization.CultureInfo( "en-CA" );
                    if (HasTime)
                    {
                        return DateTime.Value.ToString( "ddd MM-dd HH:mm", cultureInfo );
                    }
                    else
                    {
                        return DateTime.Value.ToString( "ddd MM-dd", cultureInfo );
                    }
                }

                return string.Empty;
            }
        }

        public string DateDisplay
        {
            get
            {
                if( DateTime.HasValue )
                {
                    var cultureInfo = new System.Globalization.CultureInfo( "en-CA" );
                    return DateTime.Value.ToString( "dd", cultureInfo );
                }

                return string.Empty;
            }
        }

        public string MonthShortDisplay
        {
            get
            {
                if( DateTime.HasValue )
                {
                    var cultureInfo = new System.Globalization.CultureInfo( "en-CA" );
                    return DateTime.Value.ToString( "MMM", cultureInfo );
                }

                return string.Empty;
            }
        }

        public string WeekDayDisplay
        {
            get
            {
                if( DateTime.HasValue )
                {
                    var cultureInfo = new System.Globalization.CultureInfo( "en-CA" );
                    return DateTime.Value.ToString( "dddd", cultureInfo );
                }

                return string.Empty;
            }
        }

        public string TimeDisplay
        {
            get
            {
                if( DateTime.HasValue && HasTime)
                {
                    var cultureInfo = new System.Globalization.CultureInfo( "en-CA" );
                    return DateTime.Value.ToString( "HH:mm", cultureInfo );
                }

                return string.Empty;
            }
        }
    }
}