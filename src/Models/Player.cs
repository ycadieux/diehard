﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHC.Models
{
    public class Player
    {
        public string Rank { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string GP { get; set; }
        public string Goals { get; set; }
        public string Assits { get; set; }
        public string Points { get; set; }
        public string PIM { get; set; }
        public string PPG { get; set; }
        public string SHG { get; set; }
        public string GWG { get; set; }
        public string PointsPerGame { get; set; }
        public string Playoffs { get; set; }
    }
}