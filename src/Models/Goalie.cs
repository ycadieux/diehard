﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHC.Models
{
    public class Goalie
    {
        public string Rank { get; set; }
        public string Name { get; set; }
        public string GP { get; set; }
        public string Wins { get; set; }
        public string Loses { get; set; }
        public string Ties { get; set; }
        public string GA { get; set; }
        public string SO { get; set; }
        public string SA { get; set; }
        public string AVG { get; set; }
        public string SavePourcentage { get; set; }
    }
}