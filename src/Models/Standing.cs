﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHC.Models
{
    public class Standing
    {
        public string Position { get; set; }
        public string Division { get; set; }
        public string Team { get; set; }
        public string GP { get; set; }
        public string Wins { get; set; }
        public string Loses { get; set; }
        public string Ties { get; set; }
        public string Points { get; set; }
        public string GF { get; set; }
        public string GA { get; set; }
        public string Diff { get; set; }
        public string SF { get; set; }
        public string SA { get; set; }
        public string Last5 { get; set; }

        public string GFGP
        {
            get
            {
                decimal gfgp = decimal.Parse(GF) / decimal.Parse(GP);
                return gfgp.ToString( "0.00" );
            }
        }

        public string GAGP
        {
            get
            {
                decimal gagp = decimal.Parse( GA ) / decimal.Parse( GP );
                return gagp.ToString( "0.00" );
            }
        }

        public string SFGP
        {
            get
            {
                decimal sfgp = decimal.Parse( SF ) / decimal.Parse( GP );
                return sfgp.ToString( "0.0" );
            }
        }

        public string SAGP
        {
            get
            {
                decimal sagp = decimal.Parse( SA ) / decimal.Parse( GP );
                return sagp.ToString( "0.0" );
            }
        }
    }
}